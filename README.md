# Music playback based on reaction
This code can detect your mouth and suggest music playlist based on emotions.
# Applications 🎯
This can Reduce stress and enhance relaxation when driving in traffic and other irritating situations.
# Modules
- Raspberry Pi
- Pi Camera

# Packages Used
- Tensorflow
- OpenCV
- numpy
- keras
- pygame

# Description 📌
This script will detect the mood the the driver and suggest the music playlist based on the emtions ['neutral', 'happiness', 'surprise', 'sadness', 'anger', 'disgust', 'fear'].
## Execution 🐉
```sh
live_cam_predict.py
```
